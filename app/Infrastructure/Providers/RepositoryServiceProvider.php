<?php

namespace App\Infrastructure\Providers;

use App\Modules\Invoices\Api\InvoiceRepositoryInterface;
use App\Modules\Invoices\Applications\Models\Invoice;
use App\Modules\Invoices\Applications\Repositories\InvoiceRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            InvoiceRepositoryInterface::class,
            function () {
                return new InvoiceRepository(app(Invoice::class));
            }
        );
    }
}
