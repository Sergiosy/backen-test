<?php

namespace App\Http\Controllers;

use App\Modules\Invoices\Applications\InvoiceService;
use Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct(protected InvoiceService $invoiceService, protected JsonResponse $jsonResponse)
    {
    }

    public function get(Request $request): JsonResponse
    {
        return $this->jsonResponse->setData($this->invoiceService->getInvoice($request->get('id')));
    }

    public function approve(Request $request): JsonResponse
    {
        return $this->jsonResponse->setData($this->invoiceService->approveInvoice($request->get('id')));
    }

    public function reject(Request $request): JsonResponse
    {
        return $this->jsonResponse->setData($this->invoiceService->rejectInvoice($request->get('id')));
    }
}
