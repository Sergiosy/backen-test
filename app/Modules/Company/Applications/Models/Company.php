<?php

namespace App\Modules\Company\Applications\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property  string $name
 * @property  string $street
 * @property  string $city
 * @property  string $zip_code
 * @property  string $phone
 * @property  string $email
 */
class Company extends Model
{
    protected $fillable = [
        'name',
        'street',
        'city',
        'zip_code',
        'phone',
        'email',
    ];
}
