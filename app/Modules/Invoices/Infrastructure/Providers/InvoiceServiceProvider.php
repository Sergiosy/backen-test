<?php

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Api\InvoiceRepositoryInterface;
use App\Modules\Invoices\Api\InvoiceServiceInterface;
use App\Modules\Invoices\Applications\InvoiceService;
use Illuminate\Support\ServiceProvider;

class InvoiceServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->scoped(
            InvoiceServiceInterface::class,
            function () {
                return new InvoiceService(
                    resolve(InvoiceRepositoryInterface::class),
                    resolve(ApprovalFacade::class),
                );
            }
        );
    }
}
