<?php

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Applications\Models\Invoice;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface InvoiceRepositoryInterface
{
    /**
     * @param int $id
     * @throws ModelNotFoundException
     */
    public function getById(int $id): Invoice;
}
