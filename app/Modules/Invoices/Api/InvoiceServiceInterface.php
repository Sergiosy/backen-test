<?php

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Applications\Models\Invoice;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface InvoiceServiceInterface
{
    /**
     * @param int $id
     * @throws ModelNotFoundException
     */
    public function getInvoice(int $id): Invoice;

    /**
     * @param int $id
     */
    public function approveInvoice(int $id): Invoice;

    /**
     * @param int $id
     */
    public function rejectInvoice(int $id): Invoice;
}
