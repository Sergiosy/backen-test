<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Applications\Models;

use App\Modules\Company\Applications\Models\Company;
use App\Modules\Products\Applications\Models\Products;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int    $id
 * @property string $number
 * @property string $status
 * @property int    $company_id
 * @property string $date
 * @property string $dueDate
 */
class Invoice extends Model
{
    protected $fillable = [
        'id',
        'number',
        'status',
        'date',
        'dueDate',
    ];

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    /**
     * Fake. Done just for structure. Need to realize table connector for this entity's
     */
    public function products(): HasMany
    {
        return $this->hasMany(Products::class);
    }
}
