<?php

namespace App\Modules\Invoices\Applications\DTO;

use App\Modules\Company\Applications\Models\Company;
use App\Modules\Invoices\Applications\Models\Invoice;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class InvoiceDto
{
    private int $id;

    private UuidInterface $number;

    private string $date;

    private string $dueDate;

    private Company $company;

    public function __construct(public Invoice $invoiceModel)
    {
        $this->id      = $this->invoiceModel->id;
        $this->number  = Uuid::fromString($this->invoiceModel->number);
        $this->date    = $this->invoiceModel->date;
        $this->dueDate = $this->invoiceModel->dueDate;
    }
}
