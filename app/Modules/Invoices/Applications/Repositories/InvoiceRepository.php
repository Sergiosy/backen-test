<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Applications\Repositories;

use App\Modules\Invoices\Api\InvoiceRepositoryInterface;
use App\Modules\Invoices\Applications\Models\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    /**
     * @var Invoice|Model
     */
    protected Model|Invoice $model;

    /**
     * @param Invoice $invoiceModel
     */
    public function __construct(Invoice $invoiceModel)
    {
        $this->model = $invoiceModel;
    }

    /**
     * @param int $id
     * @throws ModelNotFoundException
     */
    public function getById(int $id): Invoice
    {
        return $this->model->with('company, products')->firstOrFail($id);
    }
}
