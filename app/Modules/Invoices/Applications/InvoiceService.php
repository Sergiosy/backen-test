<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Applications;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Api\InvoiceServiceInterface;
use App\Modules\Invoices\Applications\Models\Invoice;
use App\Modules\Invoices\Applications\Repositories\InvoiceRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Ramsey\Uuid\Uuid;

class InvoiceService implements InvoiceServiceInterface
{
    public function __construct(
        protected InvoiceRepository $invoiceRepository,
        protected ApprovalFacade $approvalService,
    ) {
    }

    /**
     * @param int $id
     * @throws ModelNotFoundException
     */
    public function getInvoice(int $id): Invoice
    {
        return $this->invoiceRepository->getById($id);
    }

    /**
     * @param int $id
     */
    public function approveInvoice(int $id): Invoice
    {
        $invoice = $this->getInvoice($id);

        $this->approvalService->approve(
            new ApprovalDto(
                Uuid::fromString($invoice->number),
                StatusEnum::APPROVED,
                'invoice'
            )
        );

        if (StatusEnum::REJECTED != $invoice->status) {
            $invoice->status = StatusEnum::APPROVED;
            $invoice->save();
        }

        return $invoice;
    }

    /**
     * @param int $id
     */
    public function rejectInvoice(int $id): Invoice
    {
        $invoice = $this->getInvoice($id);

        $this->approvalService->approve(
            new ApprovalDto(
                Uuid::fromString($invoice->number),
                StatusEnum::REJECTED,
                'invoice'
            )
        );

        if (StatusEnum::APPROVED != $invoice->status) {
            $invoice->status = StatusEnum::REJECTED;
            $invoice->save();
        }

        return $invoice;
    }
}
