<?php

namespace App\Modules\Products\Applications\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 * @property int    $quantity
 * @property float  $unit_price
 * @property float  $total
 */
class Products extends Model
{
     protected $fillable = [
         'name',
         'quantity',
         'unit_price',
         'total',
     ];
}
